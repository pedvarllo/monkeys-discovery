[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)

# Discovery Service #

## Project Description ##

This is the Discovery service that allows services to find and communicate with each other
without hard-coding the hostname and port.

## Getting started ##

A Makefile has been given in order to run the application. Running the application is
very easy. All you need to do is run the following command in the root path of the project:

```make run-app```

This command also creates a Postgresql instance which is used for the different microservices.
It is known that every microservice should have a single instance, but a shared one has been
set up in order to reduce hardware requirements in the local environment. Every microservice has
its own database schema.

A Kafka environment (with Zookeeper) has been also created so as the monkeys-documents
microservice can produce events which are consumed by the monkeys-customers microservice.

This is the first microservice should be up.

## How to use this project ##

This is the first microservice should be up because it will allow services which ones
are up, and it gives the microservice names used by the API Gateway service to redirect the requests. 

## Dependencies ##

* Spring framework:
    * Spring Cloud Eureka Server
  

* Postgresql, due to the docker image.

### Authors ###

* **Pedro Varela Llorente**
    * [email](pedvarllo@gmail.com)
    * [LinkedIn](https://www.linkedin.com/in/pedvarllo/)
