
# Monkeys-tousled Application

This is the application made by Pedro Varela Llorente for The Agile Monkeys company.

This application is made by five components following the microservice architecture:

* **monkeys-discovery**, which is a Eureka Server to discover the microservices replicas.
* **monekys-api-gateway-service**, which is the API Gateway. Its main responsabilities are:
    * Get the required OAuth2 tokens to access the different microservices. In order to achieve that, it commmunicates with the IAM Provider (Keycloak).
    * It redirects every request to the appropiate microservice.
* **monkeys-users**, which manages the users created in our application. It also manages the customers in Keycloak to be able to get their tokens if needed. Only users with `ROLE_ADMIN` can create other users.
* **monkeys-customers**, which manages the customers of the application.
* **monkeys-documents**, which upload files to the system associated with the customers.


### Architecture diagram

![Scheme](./architecture.jpg)


## Installation

Clone the projects from Bitbucket in your computer.

```bash
  git clone https://pedvarllo@bitbucket.org/pedvarllo/monkeys-discovery.git
  git clone https://pedvarllo@bitbucket.org/pedvarllo/monkeys-api-gateway-service.git
  git clone https://pedvarllo@bitbucket.org/pedvarllo/monkeys-users.git
  git clone https://pedvarllo@bitbucket.org/pedvarllo/monkeys-customers.git
  git clone https://pedvarllo@bitbucket.org/pedvarllo/monkeys-documents.git
```

You will need have Docker installed in your machine to download the images and create the containers required for this project.

Maven is also required to download the different dependencies of these projects. The different dependencies for every microservice can be found in the `README.md` file in every project.
## How to setup the local environment

Every microservice contains a Makefile file in order to run. All we need to do is following these simple steps **in order**:

    1.  Run monkeys-discovery microservice.
    2.  Once monkeys-discovery is up, run monkeys-api-gateway-service microservice.
    3.  Once monkeys-api-gateway-service is up, run the other microservices (monkeys-users, monkeys-documents, monkeys-customers).

To run this microservices, you should type in console at the parent directory of every project:

```bash
make run-app
```

**Note**: To make sure you are in the correct path, a Makefile file can be found in the parent directory.

If you require further information, read the `README.md` files given in every microservice.
## API Reference

The API specification for every microservice has been given in openapi.yaml files in the path below:

`${MICROSERVICE_NAME}/src/main/resources/spec/openapi.yaml`

Copy the content file at [Swagger Editor Online](https://editor.swagger.io/) for easier reading.

## Events Reference

The `monkeys-documents` microservice uses Kafka to produce fact events as a document is uploaded. This event is consume by the **monkeys-customers** microservice to update the last path of the file uploaded and modify other customer fields.

The specification of this events is given in asyncapi.yaml file which can be found in the path below:

`${MONKEYS-DOCUMENTS}/src/main/resources/spec/asyncapi.yaml`

Copy the content file at [AsyncAPI Studio](https://studio.asyncapi.com/) for easier reading.

## Running Tests

To run tests, run the following command: 

```bash
  make test-all
```
This command will run the Unit test for every class in the project. 
For further information, please, go to the `README.md`file of the project.

### Manual testing

A Postman collection has been included, and it can be found in every microservice at the path below:

`${MICROSERVICE_PARENT_PATH}/src/main/resources/monkeys.postman_collection.json`

It includes not only the requests for the API microservice, but all the whole project. Please, import this file in your postman to help you in your testing.

If you open a request you'll find this is configured with a proper JWT token to make requests to the API. This Javascript script has been done for you to help you with testing. You can check it in the `Pre-request Script` tab in Postman.

We can see two pre-configured users to make requests:

* **monkeys-admin**, with password `admin`. This user has the `ROLE_ADMIN` role configured which allows managing users, customers and documents in the application.
* **monkeys-users**, with password `user`. This user has the `ROLE_USER` role configured which allows managing customers and documents, but no users.

Please, note that `ROLE_ADMIN` and `ROLE_USER`are the only roles possibles to create users.

Once a new user is created, you can modify the variable `options` in the Javascript script to change the username and the password in order to use it to get a proper JWT token.

The username of a user is created as follows:

```json
{
"name": "Pedro",
"surname": "Varela"
}
```
```code
username = pedro_varela
```

If you have doubts about the given username, you can check it in Keycloak:

`http://localhost:8763/auth` with the credentials given in the docker-compose.yml of the `monkeys-api-gateway-service` microservice. Once logged, click in the left menu option `Users` and click again in the `View all users` button to check it.

**Important** 

Select a new file to upload in the Postman app when you check the `monkeys-documents`microservice. 

## Considerations and improvements

### Databases

As we can see in the previous diagram, there are only two databases:

* postgresql instance for the IAM provider (Keycloak).
* postgresql instance for microservices.

In a microservice architecture, each one should have a database instance. In order to try to consume less hardware requirements in the local environment, only one database instance is given for them.
However, every microservice uses a different database schema where their tables are located.


### API Gateway pattern

Microservices should have one responsability according to their domain. For example, monkeys-users should be only responsible for managing users in the application. However, it asks for the user role provided in the JWT to authorize or not the request.

This task should be centralized in the monkeys-api-gateway-service microservice. It is a future improvement.

### Easier setup

A dockerfile could be given in order to setup the environment as indicated in previous sections.

### CI/CD

Include manifest.yml file with environment variables needed for the CI/CD flow.

### Saga Pattern

If a document cannot be uploaded to the system if an error is thrown in the `monkeys-users` (such as the given user id does not belong to any customer), this one can produce an event to indicate to the `monkeys-documents` that the document should be deleted. 
## Authors

- [Pedro Varela Llorente](https://www.linkedin.com/in/pedvarllo/)

