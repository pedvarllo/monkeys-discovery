package com.tousled.monkeys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MonkeysDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonkeysDiscoveryApplication.class, args);
	}

}
