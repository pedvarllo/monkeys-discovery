docker-down:
	sudo docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(DOCKER_PATH) down

docker-up: docker-down
	sudo docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(DOCKER_PATH) up -d

run-app: docker-up
	mvn spring-boot:run -Dspring-boot.run.arguments="--spring.profiles.active=local"


# Constants
DOCKER_PATH = ./src/main/resources/docker/docker-compose.yml
COMPOSE_PROJECT_NAME=monkeys-discovery